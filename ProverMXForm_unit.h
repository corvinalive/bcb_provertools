//---------------------------------------------------------------------------

#ifndef ProverMXForm_unitH
#define ProverMXForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
#include <ComCtrls.hpp>

#include "Prover.h"
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TProverMXForm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TLabel *Label2;
	TEdit *DescriptionEdit;
	TEdit *IndexEdit;
	TLabel *Label3;
	TNumberEdit *VEdit;
	TLabel *Label4;
	TNumberEdit *SKOEdit;
	TLabel *Label6;
	TNumberEdit *EVOEdit;
	TLabel *Label7;
	TNumberEdit *ESREdit;
	TLabel *Label8;
	TNumberEdit *ESOEdit;
	TLabel *Label9;
	TNumberEdit *ESPREdit;
	TLabel *Label10;
	TDateTimePicker *CalibrationPicker;
	TLabel *Label11;
	TDateTimePicker *EndPicker;
	TCheckBox *ValidCheckBox;
   TImage *Image1;
   TImage *Image2;
   TImage *Image3;
   TImage *Image4;
   TImage *Image5;
   TImage *Image6;
   TImage *Image7;
   TLabel *Label5;
   TNumberEdit *ESOOEdit;
   TImage *Image8;
   TLabel *Label12;
   TNumberEdit *tEdit;
   TBitBtn *Button1;
   TBitBtn *Button2;
private:
	void __fastcall EnableControls(bool Enabled);	// User declarations
protected:
	virtual void __fastcall CreateParams(Controls::TCreateParams &Params);
public:		// User declarations
	__fastcall TProverMXForm(TComponent* Owner);
	bool EditDialog(TProverMX* mx);
	void ViewDialog(TProverMX* mx);
};
//---------------------------------------------------------------------------
#endif
