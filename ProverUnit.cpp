//---------------------------------------------------------------------------
#include <ole2.h>

#pragma hdrstop

#include "ProverUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

IStorage* Root;
//---------------------------------------------------------------------------
void __fastcall LoadProvers(/*in*/IStorage* Root,/*in*/TList* Provers)
{
	IEnumSTATSTG* Enum;
	HRESULT res = Root->EnumElements(0,0,0,&Enum);
	if(res!=S_OK) return;
	Enum->Reset();
	STATSTG st;
	IMalloc* m;
	res = CoGetMalloc(1,&m);
	if(res!=S_OK)
		{
		Enum->Release();
		return;
		}
	st.pwcsName =(wchar_t*) m->Alloc(500);
	while(Enum->Next(1,&st,0)==S_OK)
		{
		IStorage* stor;
		res = Root->OpenStorage(st.pwcsName,0,OF_READ|OF_SHARE_EXCLUSIVE,0,0,&stor);
		if(res!=S_OK) continue;

		IStream* Stream;
		//try to open Stream
		HRESULT res=stor->OpenStream(WideString("prover"),0,
			OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK)
			{
			stor->Release();
			continue;
			}
		//load from stream
		TProver* p=new TProver;
		ULONG lg;
		res = Stream->Read(p,sizeof(TProver),&lg);
		if(res!=S_OK)
			delete p;
		else
			Provers->Add(p);
		Stream->Release();
		stor->Release();
		}
	m->Free(st.pwcsName);
	m->Release();
	Enum->Release();
}
//---------------------------------------------------------------------------
void __fastcall SaveProvers(/*in*/IStorage* Root,/*in*/TList* Provers)
{
	if( (!Root) || (!Provers) ) return;
	HRESULT res;
	TProver* p;
	IStorage* stor;
	IStream* Stream;
	int c=Provers->Count;
	for(int i=0;i<c;i++)
		{
		p = static_cast<TProver*>(Provers->Items[i]);
		if(!p) continue;
		WideString ws(p->ID);
		res = Root->OpenStorage(ws,0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
		if(res!=S_OK)
			{//If error open storage, try to create
			res = Root->CreateStorage(ws,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
			if(res!=S_OK)
				continue;
			}

		//try to open Stream
		res=stor->OpenStream(WideString("prover"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK)
			{
			res = stor->CreateStream(WideString("prover"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
			if(res!=S_OK)
				{
				stor->Release();
				continue;
				}
			}
		ULONG ul;
		Stream->Write(p,sizeof(TProver),&ul);
		Stream->Release();
		stor->Release();
		}
}
//---------------------------------------------------------------------------
void __fastcall DeleteProver(/*in*/IStorage* Root,/*in*/int ID)
{
	Root->DestroyElement(WideString(ID));
}
//---------------------------------------------------------------------------
void __fastcall LoadMX(/*in*/IStorage* Root,/*in*/int ProverID,/*in*/ TList* ProversMX)
{
	if( (!Root) || (!ProverID) ||(!ProversMX)) return;
	IStorage* stor;
	IStorage* mxstor;
	HRESULT res = Root->OpenStorage(WideString(ProverID),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
	if(res!=S_OK) return;
	res = stor->OpenStorage(WideString("proverMX"),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&mxstor);
	if(res!=S_OK)
		{
		stor->Release();
		return;
		}
	IEnumSTATSTG* Enum;
	res = mxstor->EnumElements(0,0,0,&Enum);
	if(res!=S_OK)
		{
		stor->Release();
		return;
		}
	Enum->Reset();
	STATSTG st;
	IMalloc* m;
	res = CoGetMalloc(1,&m);
	if(res!=S_OK)
		{
		stor->Release();
		return;
		}
	st.pwcsName =(wchar_t*) m->Alloc(500);
	IStream* Stream;
	TProverMX* mx;ULONG ul;
	while(Enum->Next(1,&st,0)==S_OK)
		{
		res=mxstor->OpenStream(st.pwcsName,0,OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK) continue;
		mx = new TProverMX;
		Stream->Read(mx,sizeof(TProverMX),&ul);
		ProversMX->Add(mx);
		Stream->Release();
		}
	mxstor->Release();
	m->Free(st.pwcsName);
	m->Release();
	Enum->Release();
	stor->Release();
	return;
}
//---------------------------------------------------------------------------
void __fastcall SaveMX(/*in*/IStorage* Root,/*in*/int ProverID,/*in*/ TList* ProversMX)
{
	if( (!Root) || (!ProverID) ||(!ProversMX) ||(ProversMX->Count==0) ) return;
	IStorage* stor;
	IStorage* mxstor;
	HRESULT res = Root->OpenStorage(WideString(ProverID),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
	if(res!=S_OK)
		{//If error open storage, try to create
		res = Root->CreateStorage(WideString(ProverID),OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
		if(res!=S_OK)
			return;
		}
	res = stor->OpenStorage(WideString("proverMX"),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&mxstor);
	if(res!=S_OK)
		{//If error open storage, try to create
		res = stor->CreateStorage(WideString("proverMX"),OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&mxstor);
		if(res!=S_OK)
			{
			stor->Release();
			return;
			}
		}

	TProverMX* mx;
	IStream* Stream;
	int c=ProversMX->Count;
	for(int i=0;i<c;i++)
		{
		mx = static_cast<TProverMX*>(ProversMX->Items[i]);
		if(!mx) continue;
		WideString ws(mx->ID);
		res=mxstor->OpenStream(ws,0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK)
			{
			res = mxstor->CreateStream(ws,OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
			if(res!=S_OK)
				{
				stor->Release();
				continue;
				}
			}
		ULONG ul;
		Stream->Write(mx,sizeof(TProverMX),&ul);
		Stream->Release();
		}
	mxstor->Release();
	stor->Release();
}
//---------------------------------------------------------------------------
void __fastcall DeleteMX(/*in*/IStorage* Root,/*in*/int ProverID,/*in*/int MX_ID)
{
	if( (!Root) || (!ProverID) ||(!MX_ID) ) return;
	IStorage* stor;
	IStorage* mxstor;
	HRESULT res = Root->OpenStorage(WideString(ProverID),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
	if(res!=S_OK) return;
	res = stor->OpenStorage(WideString("proverMX"),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&mxstor);
	if(res!=S_OK)
		{
		stor->Release();
		return;
		}
	mxstor->DestroyElement(WideString(MX_ID));
	mxstor->Release();
	stor->Release();
}
//---------------------------------------------------------------------------
void __fastcall GetProver(/*in*/int ProverID,TProver* Prover,/*in*/TList* ProversMX)
{
	if( (!Root) || (!Prover) || (!ProverID) || (!ProversMX)) return;
	HRESULT res;
	memset(Prover,0,sizeof(TProver));
	ProversMX->Clear();
	IStorage* stor;
	IStream* Stream;
/*	WideString ws;
   AnsiString s;
   s=ProverID;
   ws=s;*/
	res = Root->OpenStorage(WideString(ProverID),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
	if(res!=S_OK)
		return;
	//try to open Stream
	res=stor->OpenStream(WideString("prover"),0,OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
	if(res!=S_OK)
		{
		stor->Release();
		return;
		}
	ULONG ul;
	Stream->Read(Prover,sizeof(TProver),&ul);
	Stream->Release();
	stor->Release();
	LoadMX(Root,ProverID,ProversMX);
}
//---------------------------------------------------------------------------
void __fastcall OpenRootStorage(/*in*/IStorage* RootStorage)
{
	Root=0;
	HRESULT res = RootStorage->OpenStorage(WideString("Provers"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,
												0,0,&Root);
	if(res!=S_OK)
		{
		if(res==STG_E_FILENOTFOUND)
			RootStorage->CreateStorage(WideString("Provers"),OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&Root);
		}
}
//---------------------------------------------------------------------------
void __fastcall SaveProver(TProver* Prover)
{
	if( (!Root) || (!Prover) ) return;
	HRESULT res;
	IStorage* stor;
	IStream* Stream;
	if(Prover->ID==0) Prover->ID=rand();
	WideString ws(Prover->ID);
	res = Root->OpenStorage(ws,0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
	if(res!=S_OK)
		{//If error open storage, try to create
		res = Root->CreateStorage(ws,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
		if(res!=S_OK)
			return;
		}

	//try to open Stream
	res=stor->OpenStream(WideString("prover"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
	if(res!=S_OK)
		{
		res = stor->CreateStream(WideString("prover"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
		if(res!=S_OK)
			{
			stor->Release();
			return;
			}
		}
	ULONG ul;
	Stream->Write(Prover,sizeof(TProver),&ul);
	Stream->Release();
	stor->Release();
}
//---------------------------------------------------------------------------

