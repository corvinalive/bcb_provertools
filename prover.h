//---------------------------------------------------------------------------
#include <vcl.h>
#include <ole2.h>
#include <inifiles.hpp>

#ifndef ProversH
#define ProversH

const P_LongStr=120;
const P_ShortStr=24;
//---------------------------------------------------------------------------
class TProverMX
{
public:
	int ID;
	char Description[P_LongStr];
	char Index[P_ShortStr];
	double tBase;	//������� �����������
	double V;   //Volume
	double SKO;  //��� �������� �������� ����������� ���
	double EVO; //������� ����������� ����������� �������� ����������� ���, %
	double ESR; //������� ��������� ��������������� ������������ ����������� ���
	double ESO; //������������� ����������� ���
	double ESPR;   //������������� ���������� ����������� ��� ��� ��������� ���������� ��������
	TDate CalibrationDate;
	TDate End;
	bool ValidNow;
	double ESOO;   //������������� ���������� ����������� ��� �� �������� ��� ���������� �������
};
//---------------------------------------------------------------------------
class TProver
{
public:
enum TActionManage{New,Delete};
typedef void* __fastcall (__closure *TOnManageEvent)(TActionManage Action,
                                                      void* Data, int Reserved);
	int ID;
	enum TProverType {UniDirectional=0,BiDirectional=1,Compact=2} Type;
//	int MXCount;
	char Name[P_LongStr];
	char SN[P_ShortStr];//Serial Number
	char Location[P_LongStr];
	int Category;
	char Owner[P_LongStr];
	double Pmax;//max work pressure
	double Qmin;
	double Qmax;
	double D; //Diameter
	double S; //Thickness of Wall
	double E; //������ ��������� ��������� ������ ���
	double Alfa; //�-�� ��������� ���������� ��������� ������ ���
	//(��� �������-�������)
   double Alfa_invar;   //�-�� ��������� ���������� �����. ������� ���
   double R;   //P ������� =� � ����� / R + Padd
   double Padd;
};
//---------------------------------------------------------------------------
//#ifndef _DLL
typedef bool (*fEditProverProperties)(TComponent* Owner,/*in*/IStorage* RootStorage,TProver* Prover);
typedef void (*fViewProverProperties)(TComponent* Owner,/*in*/IStorage* RootStorage,TProver* Prover);
typedef void (*fManage)(TComponent* Owner,/*in*/IStorage* RootStorage);
typedef void (*fSelectProver)(/*in*/TComponent* Owner,/*in*/IStorage* RootStorage,
										/*out*/TProver* Prover,/*out*/ TList* MX);
typedef void (*fGetProver)(IStorage* RootStorage,/*in*/int ProverID,/*out*/TProver* Prover,/*out*/ TList* MX);
//in - ���������, ProverID;
//out - TProver*;

//#endif //_DLL
//---------------------------------------------------------------------------

#endif //ProversH
