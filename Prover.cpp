//---------------------------------------------------------------------------
#include <vcl.h>
#include <ole2.h>
#include <inifiles.hpp>

#pragma hdrstop
#include "prover.h"

//---------------------------------------------------------------------------
/*void __fastcall SaveData(IStream* Stream,void* v,int size)
{
	ULONG lg;
	HRESULT res = Stream->Write(v,size,&lg);
	if(res!=S_OK) throw(Exception("Prover.cpp\nvoid __fastcall SaveData\nres!=S_OK in Stream->Write"));
}
//---------------------------------------------------------------------------
void __fastcall LoadData(IStream* Stream,void* v,int size)
{
	ULONG lg;
	HRESULT res = Stream->Read(v,size,&lg);
	if(res!=S_OK) throw(Exception("Prover.cpp\nvoid __fastcall LoadData\nres!=S_OK in Stream->Read"));
}
//---------------------------------------------------------------------------
void __fastcall SaveStr(AnsiString* str,IStream* Stream)
{
	int c = str->Length();
	ULONG wr;
	HRESULT res = Stream->Write(&c,sizeof(int),&wr);
	if(res!=S_OK) throw(Exception("Prover.cpp\nvoid __fastcall SaveStr\nres!=S_OK in Stream->Write"));
	if(!c) return;
	res = Stream->Write(str->data(),c,&wr);
	if(res!=S_OK) throw(Exception("Prover.cpp\nvoid __fastcall SaveStr\nres!=S_OK in Stream->Write"));
};
//---------------------------------------------------------------------------
void __fastcall LoadStr(AnsiString* str,IStream* Stream)
{
	int c;
	ULONG wr;
	HRESULT res = Stream->Read(&c,sizeof(int),&wr);
	if(res!=S_OK) throw(Exception("Prover.cpp\nvoid __fastcall LoadStr\nres!=S_OK in Stream->Read c"));
	str->SetLength(c);
	if(c)
		{
		char* buf = new char[c];
		res = Stream->Read(buf,c,&wr);
		if(res!=S_OK) throw(Exception("Prover.cpp\nvoid __fastcall LoadStr\nres!=S_OK in Stream->Read buf"));
		for(int i=0;i<c;i++)
			(*str)[i+1]=buf[i];
		delete buf;
		}
};
//---------------------------------------------------------------------------
void __fastcall TProver::Save(IStorage* Storage)
{
	IStream* Stream;
	//try to open Stream
	HRESULT res=Storage->OpenStream(WideString("prover"),0,
		OF_READWRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
	if(res!=S_OK)
		{
		//if stream not found create new
		if(res==STG_E_ACCESSDENIED) Application->MessageBox("Insufficient permissions to open stream","",0);
		if(res==STG_E_FILENOTFOUND) Application->MessageBox("The stream with specified name does not exist","",0);
		if(res==STG_E_INSUFFICIENTMEMORY) Application->MessageBox("The stream was not opened due to a lack of memory","",0);
		if(res==STG_E_INVALIDFLAG) Application->MessageBox("The value specified for the grfMode flag is not a valid STGM enumeration value","",0);
		if(res==STG_E_INVALIDFUNCTION) Application->MessageBox("The specified combination of grfMode flags is not supported. For example, if this method is called without the STGM_SHARE_EXCLUSIVE flag.","",0);
		if(res==STG_E_INVALIDNAME) Application->MessageBox("Invalid value for pwcsName","",0);
		if(res==STG_E_INVALIDPOINTER) Application->MessageBox("The pointer specified for the stream object was invalid","",0);
		if(res==STG_E_INVALIDPARAMETER) Application->MessageBox("One of the parameters was invalid.","",0);
		if(res==STG_E_REVERTED) Application->MessageBox("The object has been invalidated by a revert operation above it in the transaction tree.","",0);
		if(res==STG_E_TOOMANYOPENFILES) Application->MessageBox("The stream was not opened because there are too many open files.","",0);

		if(res!=S_OK)
			{
			HRESULT res1 = Storage->CreateStream(WideString("prover"),
				OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
			if(res1!=S_OK)
				throw(Exception("void __fastcall TProver::Save(IStorage* Storage)\nError create stream for save TProver data"));
			}
		else
			throw(Exception("void __fastcall TProver::Save(IStorage* Storage)\nError open stream for save TProver data"));
		}
	//save to stream
	SaveData(Stream,&Type,sizeof(TProverType));
	SaveStr(&Name,Stream);
	SaveStr(&SN,Stream);
	SaveStr(&Location,Stream);
	SaveData(Stream,&Category,sizeof(int));
	SaveStr(&Owner,Stream);
	SaveData(Stream,&Pmax,sizeof(double));
	SaveData(Stream,&Qmin,sizeof(double));
	SaveData(Stream,&Qmax,sizeof(double));
	SaveData(Stream,&D,sizeof(double));
	SaveData(Stream,&S,sizeof(double));
	SaveData(Stream,&E,sizeof(double));
	SaveData(Stream,&Alfa,sizeof(double));
	SaveData(Stream,&Alfa_invar,sizeof(double));
	//save MX:
	int c=MXList->Count;
	SaveData(Stream,&c,sizeof(int));
	for(int i=0;i<c;i++)
		((TProverMX*)MXList->Items[i])->Save(Stream);

	Stream->Release();
};
//---------------------------------------------------------------------------
void __fastcall TProver::Load(IStorage* Storage)
{
	IStream* Stream;
	//try to open Stream
	HRESULT res=Storage->OpenStream(WideString("prover"),0,
		OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
	if(res!=S_OK)
		throw(Exception("void __fastcall TProver::Load(IStorage* Storage)\nError open stream for load TProver data"));
	//load from stream
	LoadData(Stream,&Type,sizeof(TProverType));
	LoadStr(&Name,Stream);
	LoadStr(&SN,Stream);
	LoadStr(&Location,Stream);
	LoadData(Stream,&Category,sizeof(int));
	LoadStr(&Owner,Stream);
	LoadData(Stream,&Pmax,sizeof(double));
	LoadData(Stream,&Qmin,sizeof(double));
	LoadData(Stream,&Qmax,sizeof(double));
	LoadData(Stream,&D,sizeof(double));
	LoadData(Stream,&S,sizeof(double));
	LoadData(Stream,&E,sizeof(double));
	LoadData(Stream,&Alfa,sizeof(double));
	LoadData(Stream,&Alfa_invar,sizeof(double));
//	LoadData(Stream,&LastCheck,sizeof(TDate));
	//save MX:
	int c;
	LoadData(Stream,&c,sizeof(c));
	for(int i=0;i<c;i++)
		{
		TProverMX* mx = new TProverMX;
		mx->Load(Stream);
		MXList->Add(mx);
		}

	Stream->Release();
};
//---------------------------------------------------------------------------
__fastcall TProver::TProver()
{
	MXList = new TList();
}
//---------------------------------------------------------------------------
__fastcall TProver::~TProver()
{
	int c=MXList->Count;
	for(int i=0;i<c;i++)
		delete (TProverMX*)MXList->Items[i];
	delete MXList;
}
//---------------------------------------------------------------------------
void __fastcall TProverMX::Load(IStream* Stream)
{
	LoadStr(&Description,Stream);
	LoadStr(&Index,Stream);
	LoadData(Stream,&tBase,sizeof(double));
	LoadData(Stream,&V,sizeof(double));
	LoadData(Stream,&SKO,sizeof(double));
	LoadData(Stream,&EVO,sizeof(double));
	LoadData(Stream,&ESR,sizeof(double));
	LoadData(Stream,&ESO,sizeof(double));
	LoadData(Stream,&ESPR,sizeof(double));
	LoadData(Stream,&CalibrationDate,sizeof(TDate));
	LoadData(Stream,&End,sizeof(TDate));
	LoadData(Stream,&ValidNow,sizeof(bool));
	LoadData(Stream,&ID,sizeof(int));
}
//---------------------------------------------------------------------------
void __fastcall TProverMX::Save(IStream* Stream)
{
	SaveStr(&Description,Stream);
	SaveStr(&Index,Stream);
	SaveData(Stream,&tBase,sizeof(double));
	SaveData(Stream,&V,sizeof(double));
	SaveData(Stream,&SKO,sizeof(double));
	SaveData(Stream,&EVO,sizeof(double));
	SaveData(Stream,&ESR,sizeof(double));
	SaveData(Stream,&ESO,sizeof(double));
	SaveData(Stream,&ESPR,sizeof(double));
	SaveData(Stream,&CalibrationDate,sizeof(TDate));
	SaveData(Stream,&End,sizeof(TDate));
	SaveData(Stream,&ValidNow,sizeof(bool));
	SaveData(Stream,&ID,sizeof(int));
}
//---------------------------------------------------------------------------
int __fastcall TProver::GetMXCount()
{
	return MXList->Count;
}
//---------------------------------------------------------------------------
TProverMX* __fastcall TProver::GetMX(int __index)
{
	return (TProverMX*) MXList->Items[__index];
}
//---------------------------------------------------------------------------
void __fastcall TProver::AddMX(TProverMX* mx)
{
	if(MXList->IndexOf(mx)==-1) MXList->Add(mx);
}
//---------------------------------------------------------------------------
void __fastcall TProver::ClearMX()
{
	int c=MXCount;
	for(int i=0;i<c;i++)
		delete (TProverMX*) MXList->Items[i];
	MXList->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TProver::Clear()
{
	ClearMX();
	Type=0;
	Name="";
	SN="";
	Location="";
	Category=0;
	Owner="";
	Pmax=0;
	Qmin=0;
	Qmax=0;
	D=0;
	S=0;
	E=0;
	Alfa=0;
	Alfa_invar=0;
	ClearMX();
}
//---------------------------------------------------------------------------
void __fastcall TProverMX::Clear()
{
	Description="";
	Index="";
	tBase=20;	//������� �����������
	V=0;   //Volume
	SKO=0;
	EVO=0; //������� ����������� ����������� �������� ����������� ���, %
	ESR=0; //������� ��������� ��������������� ������������ ����������� ���
	ESO=0; //������������� ����������� ���
	ESPR=0;   //������������� ���������� ����������� ��� ��� ���������
						//���������� ��������
	CalibrationDate=CalibrationDate.CurrentDate();
	End=CalibrationDate;
	End+=365;
	ValidNow=true;
}
//---------------------------------------------------------------------------
void __fastcall TProver::DeleteMX(int Index)
{
	MXList->Delete(Index);
}
//---------------------------------------------------------------------------
AnsiString __fastcall TProver::GetProverType()
{
	switch(Type)
		{
		case UniDirectional:
			return "����������������";
		case BiDirectional:
			return "���������������";
		case Compact:
			return "�������-������";
		};
	return "����������";
}
//---------------------------------------------------------------------------
*/
