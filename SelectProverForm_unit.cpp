//---------------------------------------------------------------------------

#include <vcl.h>
#include <IniFiles.hpp>
#include <ole2.h>
#include "shfolder.hpp"
#include "shlwapi.h"
#pragma hdrstop

#include "prover.h"
#include "ProverPropertiesForm_unit.h"
//#include "MXForm_unit.h"
#include "SelectProverForm_unit.h"
#include "ProverUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
AnsiString GetProverType(int i)
{
   switch(i)
      {
      case 0:
         return "����������������";
      case 1:
         return "���������������";
      case 2:
         return "�������-������";
      default:
         return "����������� ���";
      }
};
//---------------------------------------------------------------------------
__fastcall TSelectProverForm::TSelectProverForm(TComponent* Owner)
	: TForm(Owner)
{
	randomize();

	Provers = new TList();
	LoadProvers(Root,Provers);
	FillProversGrid();
	ProverPropertiesForm = new TProverPropertiesForm(this);
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::CreateParams(Controls::TCreateParams &Params)
{
	TForm::CreateParams(Params);
	TForm* f=(TForm*)Owner;
	if(f!=NULL)
		Params.WndParent=f->Handle;
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::FillProversGrid()
{
	ProversGrid->Cells[1][0]="������������";
	ProversGrid->Cells[2][0]="��� ���";
	ProversGrid->Cells[3][0]="���. �";
	ProversGrid->Cells[4][0]="������ ���";
	ProversGrid->Cells[5][0]="Pmax";
	ProversGrid->Cells[6][0]="�������� ���";
	int c=Provers->Count;
	if(c==0)
		{
		ProversGrid->RowCount=2;
		ProversGrid->Cells[0][1]="";
		ProversGrid->Cells[1][1]="";
		ProversGrid->Cells[2][1]="";
		ProversGrid->Cells[3][1]="";
		ProversGrid->Cells[4][1]="";
		ProversGrid->Cells[5][1]="";
		ProversGrid->Cells[6][1]="";
		return;
		}
	ProversGrid->RowCount=c+1;
	for(int i=0;i<c;i++)
		{
		TProver* p=((TProver*)Provers->Items[i]);
		ProversGrid->Cells[0][i+1]=i+1;
		ProversGrid->Cells[1][i+1]=p->Name;
		ProversGrid->Cells[2][i+1]=GetProverType(p->Type);
		ProversGrid->Cells[3][i+1]=p->SN;
		ProversGrid->Cells[4][i+1]=p->Category;
		ProversGrid->Cells[5][i+1]=p->Pmax;
		ProversGrid->Cells[6][i+1]=p->Owner;
		}
}
//---------------------------------------------------------------------------
__fastcall TSelectProverForm::~TSelectProverForm()
{
	delete ProverPropertiesForm;

	SaveProvers(Root,Provers);
	int c=Provers->Count;
	for(int i=0;i<c;i++)
		{
		TProver* p=static_cast<TProver*>( Provers->Items[i]);
		delete p;
		}
	delete Provers;
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::ViewButtonClick(TObject *Sender)
{
	int i=ProversGrid->Row-1;
	if((i>=0)&&(i<Provers->Count))
		{
		TProver* p = static_cast<TProver*>(Provers->Items[i]);
		ProverPropertiesForm->ViewProver(p);
		}
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::EditButtonClick(TObject *Sender)
{
	int i=ProversGrid->Row-1;
	if((i>=0)&&(i<Provers->Count))
		{
		TProver* p = static_cast<TProver*>(Provers->Items[i]);
		bool r = ProverPropertiesForm->EditProver(p);
		if(r)
			FillProversGrid();
		}
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::AddButtonClick(TObject *Sender)
{
	TProver* p=new TProver;
	memset(p,0,sizeof(TProver));
	p->ID = rand();
	if(ProverPropertiesForm->NewProver(p))
		{
		Provers->Add(p);
		FillProversGrid();
		}
	else
		delete p;
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::DeleteButtonClick(TObject *Sender)
{
	AnsiString s("������� �� ������ ���:\n\n������������\t");
	TProver* p=SelectedProver;
	if(p==NULL) return;
	s+=p->Name;
	s+="\n��� ���\t\t";
//   s+=p->GetProverType();
	s+="\n���. �\t\t";
	s+=p->SN;
	if(Application->MessageBox(s.c_str(),
		"�������� ���",MB_YESNO|MB_ICONQUESTION)==IDYES)
		{
		DeleteProver(Root,p->ID);
		Provers->Remove(p);
		delete p;
		FillProversGrid();
		}
}
//---------------------------------------------------------------------------
TProver* __fastcall TSelectProverForm::GetProverIndex()
{
	if(Provers->Count<=0) return 0;
	TProver* p = static_cast<TProver*>(Provers->Items[ProversGrid->Row-1]);
	return p;
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::Manage()
{
   HelpContext=3105;
   Caption="���������� ������� ���";
   CancelButton->Visible=false;
   OkButton->Caption="�������";
   IsManage=true;
   ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::SelectProver(TProver* Prover,/*out*/ TList* MX)
{
   HelpContext=3100;
	Caption="����� ���";
	CancelButton->Visible=true;
	OkButton->Caption="�������";
	memset(Prover,0,sizeof(TProver));
	MX->Clear();
   IsManage=false;
	bool result=(ShowModal()==mrOk);
	if(result && SelectedProver)
		{
		memcpy(Prover,SelectedProver,sizeof(TProver));
		LoadMX(Root,Prover->ID,MX);
		}
}
//---------------------------------------------------------------------------
void __fastcall TSelectProverForm::ProversGridDblClick(TObject *Sender)
{
	TProver* p=SelectedProver;
	if(p==NULL) return;
   if(IsManage)
      EditButtonClick(this);
   else
      ModalResult=mrOk;
}
//---------------------------------------------------------------------------

