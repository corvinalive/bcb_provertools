//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Prover.h"
#include "ProverMXForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TProverMXForm::TProverMXForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
bool TProverMXForm::EditDialog(TProverMX* mx)
{
	DescriptionEdit->Text=mx->Description;
	IndexEdit->Text=mx->Index;
	VEdit->Text=mx->V;
	SKOEdit->Text=mx->SKO;
	EVOEdit->Text=mx->EVO;
	ESREdit->Text=mx->ESR;
	ESOEdit->Text=mx->ESO;
	ESOOEdit->Text=mx->ESOO;
	ESPREdit->Text=mx->ESPR;
   tEdit->Text=mx->tBase;
	CalibrationPicker->Date=mx->CalibrationDate;
	EndPicker->Date=mx->End;
	ValidCheckBox->Checked=mx->ValidNow;

	Button1->Caption="��������� ��������� � �����";
	Button2->Visible=true;

	EnableControls(true);

	bool r=ShowModal()==mrOk;

	if(r)
		{
		strcpy(mx->Description,DescriptionEdit->Text.c_str());
		strcpy(mx->Index,IndexEdit->Text.c_str());
		mx->V=VEdit->Text.ToDouble();
		mx->tBase=tEdit->Text.ToDouble();
		mx->SKO=SKOEdit->Text.ToDouble();
		mx->EVO=EVOEdit->Text.ToDouble();
		mx->ESR=ESREdit->Text.ToDouble();
		mx->ESO=ESOEdit->Text.ToDouble();
		mx->ESOO=ESOOEdit->Text.ToDouble();
		mx->ESPR=ESPREdit->Text.ToDouble();
		mx->CalibrationDate=CalibrationPicker->Date;
		mx->End=EndPicker->Date;
		mx->ValidNow=ValidCheckBox->Checked;
		}
	return r;
}
//---------------------------------------------------------------------------
void TProverMXForm::ViewDialog(TProverMX* mx)
{
	Button1->Caption="�������";
	Button2->Visible=false;
	DescriptionEdit->Text=mx->Description;
	IndexEdit->Text=mx->Index;
	VEdit->Text=mx->V;
	SKOEdit->Text=mx->SKO;
	EVOEdit->Text=mx->EVO;
	ESREdit->Text=mx->ESR;
	ESOEdit->Text=mx->ESO;
	ESOOEdit->Text=mx->ESOO;
	ESPREdit->Text=mx->ESPR;
	CalibrationPicker->Date=mx->CalibrationDate;
	EndPicker->Date=mx->End;
	ValidCheckBox->Checked=mx->ValidNow;
   tEdit->Text=mx->tBase;

	EnableControls(false);

	ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TProverMXForm::EnableControls(bool Enabled)
{
	DescriptionEdit->ReadOnly=!Enabled;
	IndexEdit->ReadOnly=!Enabled;
	VEdit->ReadOnly=!Enabled;
	SKOEdit->ReadOnly=!Enabled;
	EVOEdit->ReadOnly=!Enabled;
	ESREdit->ReadOnly=!Enabled;
	ESOEdit->ReadOnly=!Enabled;
	ESOOEdit->ReadOnly=!Enabled;
	ESPREdit->ReadOnly=!Enabled;
	tEdit->ReadOnly=!Enabled;
	CalibrationPicker->Enabled=Enabled;
	EndPicker->Enabled=Enabled;
	ValidCheckBox->Enabled=Enabled;
}
//---------------------------------------------------------------------------
void __fastcall TProverMXForm::CreateParams(Controls::TCreateParams &Params)
{
   TForm::CreateParams(Params);
   TForm* f=(TForm*)Owner;
   Params.WndParent=f->Handle;
}
//---------------------------------------------------------------------------

