//---------------------------------------------------------------------------
#ifndef ProverUnitH
#define ProverUnitH

#include <ole2.h>
#include "prover.h"

//---------------------------------------------------------------------------
// ������ �� ��������� TProver � ��������� ��������� � TList
void __fastcall LoadProvers(/*in*/IStorage* Root,/*in*/TList* Provers);
void __fastcall SaveProvers(/*in*/IStorage* Root,/*in*/TList* Provers);
void __fastcall DeleteProver(/*in*/IStorage* Root,/*in*/int ID);
void __fastcall LoadMX(/*in*/IStorage* Root,/*in*/int ProverID,/*in*/TList* ProversMX);
void __fastcall SaveMX(/*in*/IStorage* Root,/*in*/int ProverID,/*in*/TList* ProversMX);
void __fastcall DeleteMX(/*in*/IStorage* Root,/*in*/int ProverID,/*in*/int MX_ID);
void __fastcall GetProver(/*in*/int ProverID,TProver* Prover,/*in*/TList* ProversMX);
void __fastcall OpenRootStorage(/*in*/IStorage* RootStorage);
void __fastcall SaveProver(TProver* Prover);

extern IStorage* Root;
//---------------------------------------------------------------------------
#endif
