//---------------------------------------------------------------------------
#include <vcl.h>
#include <windows.h>
#include <ole2.h>
#pragma hdrstop

#include "prover.h"
#include "ProverUnit.h"
#include "proverpropertiesform_unit.h"
#include "SelectProverForm_unit.h"
#include "MXForm_unit.h"

#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	if(reason==DLL_PROCESS_ATTACH)
		{
		}
	if(reason==DLL_PROCESS_DETACH)
		{
		}
	return TRUE;
}
//---------------------------------------------------------------------------

extern "C" __declspec(dllexport) bool EditProverProperties(TComponent* Owner,IStorage* RootStorage,TProver* Prover)
{
	OpenRootStorage(RootStorage);
	if(!Root) return false;
	TProverPropertiesForm* ProverPropertiesForm=new TProverPropertiesForm(Owner);
	bool result=(ProverPropertiesForm->EditProver(Prover)==mrOk);
	delete ProverPropertiesForm;
	Root->Release();
	return result;
}
//---------------------------------------------------------------------------
/*extern "C" __declspec(dllexport) bool NewProver(TComponent* Owner,TProver* Prover)
{
	ProverPropertiesForm=new TProverPropertiesForm(Owner);
	MXForm = new TMXForm(ProverPropertiesForm);
	bool result=ProverPropertiesForm->NewProver(Prover);
	delete MXForm;
	delete ProverPropertiesForm;
	return result;
}        */
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void ViewProverProperties(TComponent* Owner,IStorage* RootStorage,TProver* Prover)
{
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TProverPropertiesForm* ProverPropertiesForm=new TProverPropertiesForm(Owner);
	ProverPropertiesForm->ViewProver(Prover);
	delete ProverPropertiesForm;
	Root->Release();
}
//typedef void (*fSelectProver)(/*in*/TComponent* Owner,/*in*/IStorage* RootStorage,
 //										/*out*/;

//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void SelectProver(TComponent* Owner,IStorage* RootStorage,
															TProver* Prover,/*out*/ TList* MX)
{
	if( (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectProverForm* SelectProverForm=new TSelectProverForm(Owner);
	SelectProverForm->SelectProver(Prover,MX);
	delete SelectProverForm;
	Root->Release();
	return;
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void Manage(TComponent* Owner, IStorage* RootStorage)
{
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectProverForm* SelectProverForm=new TSelectProverForm(Owner);
	SelectProverForm->Manage();
	delete SelectProverForm;
	Root->Release();
}
//---------------------------------------------------------------------------
extern "C"  __declspec(dllexport) void GetProver(IStorage* RootStorage,/*in*/int ProverID,
                                                /*out*/TProver* Prover,/*out*/ TList* MX)
{
   if( (!RootStorage) || (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
   GetProver(ProverID,Prover,MX);
	if(!Root) return;
	Root->Release();
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void SelectProver2(TComponent* Owner,IStorage* RootStorage,
	TProver* Prover,/*out*/ TList* MX,TProver::TOnManageEvent OnManage)
{
	if( (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectProverForm* SelectProverForm=new TSelectProverForm(Owner);
   TList* TempList=new TList();
	SelectProverForm->SelectProver(Prover,TempList);
   int c=TempList->Count;
   for(int i=0;i<c;i++)
      {
      TProverMX* p;
      TProverMX* p_source;
      if(OnManage==NULL)
         //Local version
   		p=new TProverMX;
      else
         //list used an another module
   		p=(TProverMX*) OnManage(TProver::New,NULL,0);
      p_source=(TProverMX*)TempList->Items[i];
      memcpy(p,p_source,sizeof(TProverMX));
      MX->Add(p);
      delete p_source;
      }
   delete TempList;
	delete SelectProverForm;
	Root->Release();
	return;
}
//---------------------------------------------------------------------------
extern "C"  __declspec(dllexport) void GetProver2(IStorage* RootStorage,/*in*/int ProverID,
                          /*out*/TProver* Prover,/*out*/ TList* MX, TProver::TOnManageEvent OnManage)
{
   if( (!RootStorage) || (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
   TList* TempList=new TList();
   GetProver(ProverID,Prover,TempList);

   int c=TempList->Count;
   for(int i=0;i<c;i++)
      {
      TProverMX* p;
      TProverMX* p_source;
      if(OnManage==NULL)
         //Local version
   		p=new TProverMX;
      else
         //list used an another module
   		p=(TProverMX*) OnManage(TProver::New,NULL,0);
      p_source=(TProverMX*)TempList->Items[i];
      memcpy(p,p_source,sizeof(TProverMX));
      MX->Add(p);
      delete p_source;
      }
   delete TempList;

	if(!Root) return;
	Root->Release();
}
//---------------------------------------------------------------------------

