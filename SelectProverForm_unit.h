//---------------------------------------------------------------------------

#ifndef SelectProverForm_unitH
#define SelectProverForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <Grids.hpp>
#include "ProverPropertiesForm_unit.h"
//#include "MXForm_unit.h"
//---------------------------------------------------------------------------
class TSelectProverForm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TStringGrid *ProversGrid;
	TBitBtn *OkButton;
	TBitBtn *CancelButton;
   TBitBtn *ViewButton;
   TBitBtn *EditButton;
   TBitBtn *AddButton;
   TBitBtn *DeleteButton;
	void __fastcall ViewButtonClick(TObject *Sender);
	void __fastcall EditButtonClick(TObject *Sender);
	void __fastcall AddButtonClick(TObject *Sender);
	void __fastcall DeleteButtonClick(TObject *Sender);
   void __fastcall ProversGridDblClick(TObject *Sender);
private:	// User declarations
	TProverPropertiesForm* ProverPropertiesForm;
   bool IsManage;
//      char FN[360];
protected:
	TList* Provers;
	TList* DeletedProvers;
	TProver* __fastcall GetProverIndex();
	virtual void __fastcall CreateParams(Controls::TCreateParams &Params);
	void __fastcall FillProversGrid();
public:		// User declarations
	__property TProver* SelectedProver  = { read=GetProverIndex };
	__fastcall TSelectProverForm(TComponent* Owner);
   __fastcall ~TSelectProverForm();
   void __fastcall Manage();
	void __fastcall SelectProver(TProver* Prover,/*out*/ TList* MX);
};
//---------------------------------------------------------------------------
#endif
