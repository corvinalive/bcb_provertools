//---------------------------------------------------------------------------
#include <vcl.h>
#include "Prover.h"

#ifndef ProverDLLInterfaceH
#define ProverDLLInterfaceH
//---------------------------------------------------------------------------
class TProverDLLInterface
{
protected:
 
private:
   void* __fastcall ManageProverMX(TProver::TActionManage Action, void* Data, int Reserved);
//������� ������ 1.0:


public:
   __fastcall TProverDLLInterface(HINSTANCE DLLHandle);
   bool __fastcall Edit(TComponent* Owner, IStorage* RootStorage, TProver* TProver);
   void __fastcall View(TComponent* Owner, IStorage* RootStorage, TProver* TProver);
   void __fastcall Manage(TComponent* Owner, IStorage* RootStorage);
   void __fastcall Select(TComponent* Owner, IStorage* RootStorage, TProver* TProver, TList* ProverMXList);
   void __fastcall Get(IStorage* RootStorage, int ID, TProver* TProver, TList* ProverMXList);
   __fastcall TProverDLLInterface();
};
//---------------------------------------------------------------------------

#endif
