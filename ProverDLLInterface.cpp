//---------------------------------------------------------------------------


#pragma hdrstop

#include "ProverDLLInterface.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#include "prover.h"
#include "ProverUnit.h"
#include "proverpropertiesform_unit.h"
#include "SelectProverForm_unit.h"
//#include "MXForm_unit.h"

#pragma argsused

bool EditProverProperties(TComponent* Owner,IStorage* RootStorage,TProver* Prover)
{
	OpenRootStorage(RootStorage);
	if(!Root) return false;
	TProverPropertiesForm* ProverPropertiesForm=new TProverPropertiesForm(Owner);
	bool result=(ProverPropertiesForm->EditProver(Prover)==mrOk);
	delete ProverPropertiesForm;
	Root->Release();
	return result;
}
//---------------------------------------------------------------------------
void ViewProverProperties(TComponent* Owner,IStorage* RootStorage,TProver* Prover)
{
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TProverPropertiesForm* ProverPropertiesForm=new TProverPropertiesForm(Owner);
	ProverPropertiesForm->ViewProver(Prover);
	delete ProverPropertiesForm;
	Root->Release();
}
//---------------------------------------------------------------------------
void SelectProver(TComponent* Owner,IStorage* RootStorage,
															TProver* Prover,/*out*/ TList* MX)
{
	if( (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectProverForm* SelectProverForm=new TSelectProverForm(Owner);
	SelectProverForm->SelectProver(Prover,MX);
	delete SelectProverForm;
	Root->Release();
	return;
}
//---------------------------------------------------------------------------
void Manageit(TComponent* Owner, IStorage* RootStorage)
{
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectProverForm* SelectProverForm=new TSelectProverForm(Owner);
	SelectProverForm->Manage();
	delete SelectProverForm;
	Root->Release();
}
//---------------------------------------------------------------------------
void GetProver(IStorage* RootStorage,/*in*/int ProverID,
                                                /*out*/TProver* Prover,/*out*/ TList* MX)
{
   if( (!RootStorage) || (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
   GetProver(ProverID,Prover,MX);
	if(!Root) return;
	Root->Release();
}
//---------------------------------------------------------------------------
void SelectProver2(TComponent* Owner,IStorage* RootStorage,
	TProver* Prover,/*out*/ TList* MX,TProver::TOnManageEvent OnManage)
{
	if( (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectProverForm* SelectProverForm=new TSelectProverForm(Owner);
   TList* TempList=new TList();
	SelectProverForm->SelectProver(Prover,TempList);
   int c=TempList->Count;
   for(int i=0;i<c;i++)
      {
      TProverMX* p;
      TProverMX* p_source;
      if(OnManage==NULL)
         //Local version
   		p=new TProverMX;
      else
         //list used an another module
   		p=(TProverMX*) OnManage(TProver::New,NULL,0);
      p_source=(TProverMX*)TempList->Items[i];
      memcpy(p,p_source,sizeof(TProverMX));
      MX->Add(p);
      delete p_source;
      }
   delete TempList;
	delete SelectProverForm;
	Root->Release();
	return;
}
//---------------------------------------------------------------------------
void GetProver2(IStorage* RootStorage,/*in*/int ProverID,
                          /*out*/TProver* Prover,/*out*/ TList* MX, TProver::TOnManageEvent OnManage)
{
   if( (!RootStorage) || (!Prover) || (!MX) ) return;
	OpenRootStorage(RootStorage);
   TList* TempList=new TList();
   GetProver(ProverID,Prover,TempList);

   int c=TempList->Count;
   for(int i=0;i<c;i++)
      {
      TProverMX* p;
      TProverMX* p_source;
      if(OnManage==NULL)
         //Local version
   		p=new TProverMX;
      else
         //list used an another module
   		p=(TProverMX*) OnManage(TProver::New,NULL,0);
      p_source=(TProverMX*)TempList->Items[i];
      memcpy(p,p_source,sizeof(TProverMX));
      MX->Add(p);
      delete p_source;
      }
   delete TempList;

	if(!Root) return;
	Root->Release();
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
__fastcall TProverDLLInterface::TProverDLLInterface()
{
}
//---------------------------------------------------------------------------
bool __fastcall TProverDLLInterface::Edit(TComponent* Owner, IStorage* RootStorage, TProver* Prover)
{
   return EditProverProperties(Owner,RootStorage,Prover);
}
//---------------------------------------------------------------------------
void __fastcall TProverDLLInterface::View(TComponent* Owner, IStorage* RootStorage, TProver* Prover)
{
   ViewProverProperties(Owner,RootStorage,Prover);
}
//---------------------------------------------------------------------------
void __fastcall TProverDLLInterface::Manage(TComponent* Owner, IStorage* RootStorage)
{
   Manageit(Owner,RootStorage);
}
//---------------------------------------------------------------------------
void __fastcall TProverDLLInterface::Select(TComponent* Owner, IStorage* RootStorage, TProver* Prover,TList* ProverMXList)
{

      SelectProver2(Owner,RootStorage,Prover, ProverMXList,&ManageProverMX);

}
//---------------------------------------------------------------------------
void __fastcall TProverDLLInterface::Get(IStorage* RootStorage, int ID, TProver* Prover, TList* ProverMXList)
{

      GetProver2(RootStorage,ID,Prover,ProverMXList,&ManageProverMX);
      
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void* __fastcall TProverDLLInterface::ManageProverMX(TProver::TActionManage Action, void* Data, int Reserved)
{
   if(Action==TProver::New)
      {
      return new TProverMX();
      }
   if(Action==TProver::Delete)
      {
      delete (TProver*) Data;
      return NULL;
      }
   return NULL;
}
//---------------------------------------------------------------------------






