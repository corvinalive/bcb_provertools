//---------------------------------------------------------------------------

#ifndef ProverPropertiesForm_unitH
#define ProverPropertiesForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
#include <ComCtrls.hpp>
#include "ProverMXForm_unit.h"
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Buttons.hpp>
#include <ImgList.hpp>

#include <Menus.hpp>
//---------------------------------------------------------------------------
class TProverPropertiesForm : public TForm
{
__published:	// IDE-managed Components
   TGroupBox *GroupBox1;
   TLabel *Label1;
   TLabel *Label3;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TLabel *Label8;
   TLabel *Label9;
   TLabel *Label10;
   TLabel *Label11;
   TLabel *Label12;
   TLabel *Label13;
   TLabel *Label14;
   TLabel *Label15;
   TEdit *NameEdit;
   TComboBox *TypeComboBox;
	TEdit *SNEdit;
	TEdit *LocationEdit;
	TEdit *OwnerEdit;
	TNumberEdit *QminEdit;
	TNumberEdit *QmaxEdit;
	TNumberEdit *DEdit;
	TNumberEdit *SEdit;
	TNumberEdit *EEdit;
	TNumberEdit *AlfaEdit;
	TNumberEdit *Alfa_invarEdit;
	TLabel *Label26;
	TNumberEdit *PmaxEdit;
	TGroupBox *GroupBox2;
	TListBox *MXListBox;
	TNumberEdit *CategoryEdit;
   TImage *Image1;
   TImage *Image2;
   TImage *Image3;
   TLabel *Label2;
   TLabel *Label4;
   TLabel *Label16;
   TImage *Image4;
   TImage *Image5;
   TLabel *Label17;
   TImage *Image6;
   TLabel *Label18;
   TNumberEdit *REdit;
   TNumberEdit *PminEdit;
   TBitBtn *OKButton;
   TBitBtn *CancelButton;
   TBitBtn *EditMXButton;
   TBitBtn *DeleteMXButton;
   TBitBtn *AddMXButton;
   TImageList *ImageList1;
   TPopupMenu *PopupMenu1;
   TMenuItem *N210001003005001;
   TMenuItem *MenuPopupE;
   TPopupMenu *PopupMenu2;
   TMenuItem *N2100001003005001;
   TMenuItem *N000002161741;
   TPopupMenu *PopupMenu3;
   TMenuItem *N144E061;
   TMenuItem *N000001116Daniel1;
   TMenuItem *N207000Daniel1;
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
	void __fastcall OKButtonClick(TObject *Sender);
	void __fastcall AddMXButtonClick(TObject *Sender);
	void __fastcall EditMXButtonClick(TObject *Sender);
	void __fastcall DeleteMXButtonClick(TObject *Sender);
   void __fastcall MXListBoxDrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
   void __fastcall N210001003005001Click(TObject *Sender);
   void __fastcall MenuPopupEClick(TObject *Sender);
   void __fastcall N2100001003005001Click(TObject *Sender);
   void __fastcall N000002161741Click(TObject *Sender);
   void __fastcall N144E061Click(TObject *Sender);
   void __fastcall N000001116Daniel1Click(TObject *Sender);
   void __fastcall N207000Daniel1Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
private:
	void __fastcall FillMXList();	// User declarations
protected:
	virtual void __fastcall CreateParams(Controls::TCreateParams &Params);
	void __fastcall EnableControls();
	bool CanEdit;
	bool IsNew;
	int ID;
//	TProver Prover;
	TList* MX;
	TList* DeletedMX;
	void __fastcall FillMX(int i);
	void __fastcall EnableMXControls(bool Enabled);
	void __fastcall ProverToForm(TProver* Prover);
	void __fastcall FormToProver(TProver* Prover);
public:		// User declarations
	__fastcall TProverPropertiesForm(TComponent* Owner);
	bool __fastcall NewProver(TProver* Prover);
	void __fastcall ViewProver(TProver* Prover);
	bool __fastcall EditProver(TProver* Prover);
	__fastcall ~TProverPropertiesForm();
	void __fastcall FillMXData();
	void __fastcall ClearMXData();
	void __fastcall SaveMXData();
	TProverMXForm* MXForm;
};
//---------------------------------------------------------------------------
#endif
