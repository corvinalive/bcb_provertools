//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "prover.h"
#include "ProverPropertiesForm_unit.h"
#include "ProverMXForm_unit.h"
#include "ProverUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"


#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TProverPropertiesForm::TProverPropertiesForm(TComponent* Owner)
	: TForm(Owner)
{
	MX = new TList;
	DeletedMX = new TList;
	MXForm = new TProverMXForm(Owner);
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::CreateParams(Controls::TCreateParams &Params)
{
   TForm::CreateParams(Params);
   TForm* f=(TForm*)Owner;
   Params.WndParent=f->Handle;
}
//---------------------------------------------------------------------------
bool __fastcall TProverPropertiesForm::NewProver(TProver* p)
{
   HelpContext=3102;
	Caption="���������� ���";
	OKButton->Caption="��������� ��������� � �����";
	CancelButton->Visible=true;
	CancelButton->Caption="����� ��� ���������� ���������";
	CanEdit=true;
	IsNew=true;
	//
	EnableControls();
	ClearMXData();
	ProverToForm(p);
	if(ShowModal()==mrOk)
		{
		FormToProver(p);
		SaveMXData();
		ClearMXData();
		return true;
		}
	else
		return false;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::EnableControls()
{
   //JvArrowButton1->Visible=CanEdit;
   //JvArrowButton2->Visible=CanEdit;
   //JvArrowButton3->Visible=CanEdit;
   NameEdit->ReadOnly=!CanEdit;
   TypeComboBox->Enabled=CanEdit;
   CategoryEdit->ReadOnly=!CanEdit;
   SNEdit->ReadOnly=!CanEdit;
   LocationEdit->ReadOnly=!CanEdit;
   OwnerEdit->ReadOnly=!CanEdit;
   PmaxEdit->ReadOnly=!CanEdit;
   QminEdit->ReadOnly=!CanEdit;
   QmaxEdit->ReadOnly=!CanEdit;
   DEdit->ReadOnly=!CanEdit;
   SEdit->ReadOnly=!CanEdit;
   EEdit->ReadOnly=!CanEdit;
   REdit->ReadOnly=!CanEdit;
   PminEdit->ReadOnly=!CanEdit;
   AlfaEdit->ReadOnly=!CanEdit;
	Alfa_invarEdit->ReadOnly=!CanEdit;
	AddMXButton->Enabled=CanEdit;
	DeleteMXButton->Enabled=CanEdit;
	if(CanEdit)
      {
		EditMXButton->Caption="������������� ���������� �������...";
      ImageList1->GetBitmap(1,EditMXButton->Glyph);
      }
	else
      {
		EditMXButton->Caption="����������� ���������� �������...";
      ImageList1->GetBitmap(0,EditMXButton->Glyph);
      }
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::OKButtonClick(TObject *Sender)
{
	if(!CanEdit) return;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::ProverToForm(TProver* p)
{
	ID=p->ID;
	NameEdit->Text=p->Name;
	TypeComboBox->ItemIndex=p->Type;
	CategoryEdit->Text=p->Category;
	SNEdit->Text=p->SN;
	LocationEdit->Text=p->Location;
	OwnerEdit->Text=p->Owner;
	PmaxEdit->Text=p->Pmax;
	QminEdit->Text=p->Qmin;
	QmaxEdit->Text=p->Qmax;
	DEdit->Text=p->D;
	SEdit->Text=p->S;
	EEdit->Text=p->E;
	REdit->Text=p->R;
	PminEdit->Text=p->Padd;
	AlfaEdit->Text=p->Alfa;
	Alfa_invarEdit->Text=p->Alfa_invar;
	FillMXData();

	FillMXList();
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::ViewProver(TProver* p)
{
   HelpContext=3101;
	Caption="�������� ������ ���";
	OKButton->Caption="�������";
	CancelButton->Visible=false;
	CanEdit=false;
	IsNew=false;
	EnableControls();
	ProverToForm(p);
	ShowModal();
	ClearMXData();
}
//---------------------------------------------------------------------------
bool __fastcall TProverPropertiesForm::EditProver(TProver* p)
{
   HelpContext=3102;
	Caption="�������������� ������ ���";
	OKButton->Caption="��������� ��������� � �����";
	CancelButton->Visible=true;
	CancelButton->Caption="����� ��� ���������� ���������";
	CanEdit=true;
	IsNew=false;
	EnableControls();
	ProverToForm(p);
	if(ShowModal()==mrOk)
		{
		FormToProver(p);
		SaveProver(p);
		SaveMXData();
		ClearMXData();
		return true;
		}
	ClearMXData();
	return false;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::FormToProver(TProver* p)
{
	p->ID=ID;
	strcpy(p->Name,NameEdit->Text.c_str());
	p->Type=(TProver::TProverType)TypeComboBox->ItemIndex;
	p->Category=CategoryEdit->Text.ToInt();
	strcpy(p->SN,SNEdit->Text.c_str());
	strcpy(p->Location,LocationEdit->Text.c_str());
	strcpy(p->Owner,OwnerEdit->Text.c_str());

	p->Pmax=PmaxEdit->Text.ToDouble();
	p->Qmin=QminEdit->Text.ToDouble();
	p->Qmax=QmaxEdit->Text.ToDouble();
	p->D=DEdit->Text.ToDouble();
	p->S=SEdit->Text.ToDouble();
	p->E=EEdit->Text.ToDouble();
	p->R=REdit->Text.ToDouble();
	p->Padd=PminEdit->Text.ToDouble();
	p->Alfa=AlfaEdit->Text.ToDouble();
	p->Alfa_invar=Alfa_invarEdit->Text.ToDouble();
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::AddMXButtonClick(TObject *Sender)
{
	TProverMX* mx = new TProverMX;
	memset(mx,0,sizeof(TProverMX));
   mx->tBase=20;
   mx->CalibrationDate=mx->CalibrationDate.CurrentDate();
   mx->End=mx->CalibrationDate+365;
	if(MXForm->EditDialog(mx))
		{
		mx->ID=rand();
		MX->Add(mx);
		FillMXList();
		}
	else delete mx;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::FillMXList()
{
	int c=MX->Count;
//	MXListBox->Clear();
   MXListBox->Count=c;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::EditMXButtonClick(TObject *Sender)
{
	TProverMX* mx;
	int c=MXListBox->ItemIndex;
	if(c==-1) return;
	mx=static_cast<TProverMX*> (MX->Items[c]);
	if(mx==NULL) return;
	if(CanEdit)
		{
		if(MXForm->EditDialog(mx))
			FillMXList();
		}
	else
		MXForm->ViewDialog(mx);  
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::DeleteMXButtonClick(TObject *Sender)
{
	TProverMX* mx;
	int c=MXListBox->ItemIndex;
	if(c==-1) return;
	if(c>=MX->Count) return;
	mx=static_cast<TProverMX*>(MX->Items[c]);
	if(!mx) return;
	AnsiString s("������� ���������� �������:\n");
	s+="\t";
	s+=mx->Description;
	s+="\n\t���� ������� ";
	s+=mx->CalibrationDate.DateString();
	if(MessageBox(Handle,s.c_str(),"�������� ����������� �������",MB_YESNO|MB_ICONSTOP)==ID_YES)
		{
//		DeleteMX(Root,ID,mx->ID);
		MX->Extract(mx);
      DeletedMX->Add(mx);
		FillMXList();
		}
}
//---------------------------------------------------------------------------
__fastcall TProverPropertiesForm::~TProverPropertiesForm()
{
	delete MX;
	delete MXForm;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::FillMXData()
{
	if( (ID<=0) || (Root==0) ) return;
	ClearMXData();
	LoadMX(Root,ID,MX);
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::ClearMXData()
{
	TProverMX* mx;
	int c = MX->Count;
	for(int i=0;i<c;i++)
		{
		mx=static_cast<TProverMX*>(MX->Items[i]);
		delete mx;
		}
	MX->Clear();
	c = DeletedMX->Count;
	for(int i=0;i<c;i++)
		{
		mx=static_cast<TProverMX*>(DeletedMX->Items[i]);
		delete mx;
		}
	DeletedMX->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::SaveMXData()
{
	SaveMX(Root,ID,MX);
	TProverMX* mx;
	int c = DeletedMX->Count;
	for(int i=0;i<c;i++)
		{
		mx=static_cast<TProverMX*>(DeletedMX->Items[i]);
		DeleteMX(Root,ID,mx->ID);
		}
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::MXListBoxDrawItem(
      TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
   TProverMX* mx = static_cast<TProverMX*>(MX->Items[Index]);
   if(State.Contains(odSelected))
      MXListBox->Canvas->Brush->Color=clSkyBlue;
   else
      MXListBox->Canvas->Brush->Color=clWhite;
   MXListBox->Canvas->FillRect(Rect);
   AnsiString s="���� �������: ";
   s+=mx->CalibrationDate.DateString();
   if(mx->ValidNow)
      MXListBox->Canvas->Font->Style=TFontStyles()<< fsBold;
   else
      MXListBox->Canvas->Font->Style=TFontStyles();
   MXListBox->Canvas->TextOut(Rect.left+2,Rect.top+2+14,s);
   s="���������: ";
   s+=mx->Index;
   MXListBox->Canvas->TextOut(Rect.left+2,Rect.top+2,s);

}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::N210001003005001Click(
      TObject *Sender)
{
   EEdit->Text=210000;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::MenuPopupEClick(TObject *Sender)
{
   EEdit->Text=196500;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::N2100001003005001Click(
      TObject *Sender)
{
   AlfaEdit->Text=0.0000112;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::N000002161741Click(TObject *Sender)
{
   AlfaEdit->Text=0.0000216;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::N144E061Click(TObject *Sender)
{
   Alfa_invarEdit->Text=0.00000144;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::N000001116Daniel1Click(
      TObject *Sender)
{
   AlfaEdit->Text=0.00001116;
}
//---------------------------------------------------------------------------
void __fastcall TProverPropertiesForm::N207000Daniel1Click(TObject *Sender)
{
   EEdit->Text=207000;
}
//---------------------------------------------------------------------------

void __fastcall TProverPropertiesForm::Button1Click(TObject *Sender)
{
        Types::TPoint p=Button1->ClientOrigin   ;
        PopupMenu1->Popup(p.x, p.y)    ;
}
//---------------------------------------------------------------------------

void __fastcall TProverPropertiesForm::Button2Click(TObject *Sender)
{
        Types::TPoint p=Button2->ClientOrigin   ;
        PopupMenu2->Popup(p.x, p.y)    ;
}
//---------------------------------------------------------------------------

void __fastcall TProverPropertiesForm::Button3Click(TObject *Sender)
{
        Types::TPoint p=Button3->ClientOrigin   ;
        PopupMenu3->Popup(p.x, p.y)    ;
}
//---------------------------------------------------------------------------

