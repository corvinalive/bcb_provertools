object ProverMXForm: TProverMXForm
  Left = 291
  Top = 181
  HelpContext = 3104
  ActiveControl = DescriptionEdit
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1087#1086#1074#1077#1088#1082#1080' '#1058#1055#1059
  ClientHeight = 279
  ClientWidth = 550
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000E000000000000000EEEEEEEEEEE00000EAAAAAAAAAAE0000EEE
    EEEEEEEAAE000E000000000EAAE0000000000000EAE0000000000000EAE00000
    00000000EAE0000000000000EAE00E000000000EAAE00EEEEEEEEEEAAE000EAA
    AAAAAAAAE0000EEEEEEEEEEE00000E000000000000000000000000000000FFFF
    0000BFFF0000800F00008007000080030000BFE10000FFF10000FFF10000FFF1
    0000FFF10000BFE100008003000080070000800F0000BFFF0000FFFF0000}
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 8
    Width = 179
    Height = 13
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1087#1072#1088#1099' '#1076#1077#1090#1077#1082#1090#1086#1088#1086#1074
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 154
    Height = 13
    Caption = #1050#1088#1072#1090#1082#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1076#1077#1090#1077#1082#1090#1086#1088#1086#1074
  end
  object Label3: TLabel
    Left = 36
    Top = 104
    Width = 163
    Height = 13
    Caption = #1054#1073#1098#1077#1084' '#1082#1072#1083#1080#1073#1088#1086#1074#1072#1085#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
  end
  object Label4: TLabel
    Left = 46
    Top = 146
    Width = 125
    Height = 26
    Caption = #1057#1050#1054' '#1089#1088#1077#1076#1085#1077#1075#1086' '#1079#1085#1072#1095#1077#1085#1080#1103' '#1074#1084#1077#1089#1090#1080#1084#1086#1089#1090#1080' '#1058#1055#1059
    WordWrap = True
  end
  object Label6: TLabel
    Left = 238
    Top = 20
    Width = 232
    Height = 26
    Caption = #1075#1088#1072#1085#1080#1094#1072' '#1087#1086#1075#1088#1077#1096#1085#1086#1089#1090#1080' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103' '#1079#1085#1072#1095#1077#1085#1080#1103' '#1074#1084#1077#1089#1090#1080#1084#1086#1089#1090#1080' '#1058#1055#1059
    WordWrap = True
  end
  object Label7: TLabel
    Left = 244
    Top = 54
    Width = 196
    Height = 26
    Caption = #1075#1088#1072#1085#1080#1094#1072' '#1089#1091#1084#1084#1072#1088#1085#1086#1081' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1095#1077#1089#1082#1086#1081' '#1089#1086#1089#1090#1072#1074#1083#1103#1102#1097#1077#1081' '#1087#1086#1075#1088#1077#1096#1085#1086#1089#1090#1080' '#1058#1055#1059
    WordWrap = True
  end
  object Label8: TLabel
    Left = 236
    Top = 88
    Width = 171
    Height = 13
    Caption = #1086#1090#1085#1086#1089#1080#1090#1077#1083#1100#1085#1072#1103' '#1087#1086#1075#1088#1077#1096#1085#1086#1089#1090#1100' '#1058#1055#1059
  end
  object Label9: TLabel
    Left = 240
    Top = 106
    Width = 213
    Height = 26
    Caption = 
      #1086#1090#1085#1086#1089#1080#1090#1077#1083#1100#1085#1086#1077' '#1086#1090#1082#1083#1086#1085#1077#1085#1080#1077' '#1074#1084#1077#1089#1090#1080#1084#1086#1089#1090#1080' '#1058#1055#1059' '#1087#1088#1080' '#1088#1072#1079#1083#1080#1095#1085#1099#1093' '#1087#1086#1074#1077#1088#1086#1095#1085#1099 +
      #1093' '#1088#1072#1089#1093#1086#1076#1072#1093
    WordWrap = True
  end
  object Label10: TLabel
    Left = 4
    Top = 202
    Width = 71
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1074#1077#1088#1082#1080
  end
  object Label11: TLabel
    Left = 150
    Top = 202
    Width = 160
    Height = 13
    Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103' '#1089#1088#1086#1082#1072' '#1087#1086#1074#1077#1088#1082#1080
  end
  object Image1: TImage
    Left = 8
    Top = 98
    Width = 18
    Height = 21
    AutoSize = True
    Picture.Data = {
      07544269746D617092000000424D92000000000000003E000000280000001200
      0000150000000100010000000000540000000000000000000000020000000000
      000000000000FFFFFF00FFFFC000FFFFC000FFFFC000FFF3C000FFEDC000FFED
      C000FFEDC000FEEDC000FCEDC000FD73C000FD7FC000FBBFC000FBBFC000FBBF
      C000F7DFC000F7DFC000C387C000FFFFC000FFFFC000FFFFC000FFFFC000}
    Transparent = True
  end
  object Image2: TImage
    Left = 206
    Top = 24
    Width = 27
    Height = 21
    AutoSize = True
    Picture.Data = {
      07544269746D617092000000424D92000000000000003E000000280000001B00
      0000150000000100010000000000540000000000000000000000020000000000
      000000000000FFFFFF00FFFFFFE0FFFFFFE0FFFFFFE0FFFF79E0FFFEB6E0FFFE
      B6E0FFFEB6E0FC3DD6E0F3CDD6E0F7E8C9E0EFF7FFE0EBD7FFE0E817FFE0EBD7
      FFE0EFF7FFE0F7EFFFE0F3CFFFE0FC3FFFE0FFFFFFE0FFFFFFE0FFFFFFE0}
    Transparent = True
  end
  object Image3: TImage
    Left = 208
    Top = 58
    Width = 27
    Height = 21
    AutoSize = True
    Picture.Data = {
      07544269746D617092000000424D92000000000000003E000000280000001B00
      0000150000000100010000000000540000000000000000000000020000000000
      000000000000FFFFFF00FFFFFFE0FFFFFFE0FFFFFFE0FFF839E0FFFDB6E0FFFE
      F6E0FFFEF6E0FC3DF6E0F3CDB6E0F7E839E0EFF7FFE0EBD7FFE0E817FFE0EBD7
      FFE0EFF7FFE0F7EFFFE0F3CFFFE0FC3FFFE0FFFFFFE0FFFFFFE0FFFFFFE0}
    Transparent = True
  end
  object Image4: TImage
    Left = 6
    Top = 142
    Width = 38
    Height = 31
    AutoSize = True
    Picture.Data = {
      07544269746D617036010000424D36010000000000003E000000280000002600
      00001F0000000100010000000000F80000000000000000000000020000000000
      000000000000FFFFFF00FFFFFFFFFC000000FFFFFFFFFC000000FFFFFFFFFC00
      0000FFE7FFFFFC000000FFDBF7FEFC000000FFDBEFFF7C000000FFDBDFFFBC00
      0000D1DBDC01BC000000CEDBBDFBDC000000DF67BEFBDC000000FF7FBEF7DC00
      0000FEFFBF77DC000000F9FFBF77DC000000E7FFBF6FDC000000DFFFDFAFBC00
      0000DF7FDFAFBC000000EE7FEFDF7C000000F17FF7DEFC000000FFFFFFFFFC00
      0000FFFFFFFFFC000000FFFFFFFFFC000000FFFFFFFFFC000000FFFFFF8FFC00
      0000FFFFFF6FFC000000FFFFFF9FFC000000FFFFFFFFFC000000FFFFFFFFFC00
      0000FFFFFFFFFC000000FFFFFFFFFC000000FFFFFFFFFC000000FFFFFFFFFC00
      0000}
    Transparent = True
  end
  object Image5: TImage
    Left = 208
    Top = 140
    Width = 24
    Height = 21
    AutoSize = True
    Picture.Data = {
      07544269746D617092000000424D92000000000000003E000000280000001800
      0000150000000100010000000000540000000000000000000000020000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF9CF00FFF6B700FFF6
      B700FFF6B700E006B700EFE6B700F7D9CF00F7DFFF00FBBFFF00FBBFFF00FD7F
      FF00FD7FFF00FEFFFF00FEFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
    Transparent = True
  end
  object Image6: TImage
    Left = 208
    Top = 84
    Width = 18
    Height = 21
    AutoSize = True
    Picture.Data = {
      07544269746D617092000000424D92000000000000003E000000280000001200
      0000150000000100010000000000540000000000000000000000020000000000
      000000000000FFFFFF00FFFFC000FFFFC000FFFFC000FFF9C000FFF6C000FFF6
      C000FFF6C000C006C000EFE6C000EFE9C000F7DFC000F7DFC000FBBFC000FBBF
      C000FD7FC000FD7FC000FEFFC000FEFFC000FFFFC000FFFFC000FFFFC000}
    Transparent = True
  end
  object Image7: TImage
    Left = 208
    Top = 110
    Width = 25
    Height = 23
    AutoSize = True
    Picture.Data = {
      07544269746D61709A000000424D9A000000000000003E000000280000001900
      00001700000001000100000000005C0000000000000000000000020000000000
      000000000000FFFFFF00FFFFFF80FFFFFF80FFFFFF80FFFF1F80FFFFBF80FFE2
      0F80FFF6B780FFF6B780FFF6B780E0000F80EFEFFF80F7DFFF80F7DFFF80FBBF
      FF80FBBFFF80FD7FFF80FD7FFF80FEFFFF80FEFFFF80FFFFFF80FFFFFF80FFFF
      FF80FFFFFF80}
    Transparent = True
  end
  object Label5: TLabel
    Left = 240
    Top = 136
    Width = 220
    Height = 26
    Caption = 
      #1086#1090#1085#1086#1089#1080#1090#1077#1083#1100#1085#1086#1077' '#1086#1090#1082#1083#1086#1085#1077#1085#1080#1077' '#1074#1084#1077#1089#1090#1080#1084#1086#1089#1090#1080' '#1058#1055#1059' '#1086#1090' '#1079#1085#1072#1095#1077#1085#1080#1103' '#1087#1088#1080' '#1087#1088#1077#1076#1099#1076#1091 +
      #1097#1077#1081' '#1087#1086#1074#1077#1088#1082#1077
    WordWrap = True
  end
  object Image8: TImage
    Left = 210
    Top = 168
    Width = 14
    Height = 21
    AutoSize = True
    Picture.Data = {
      07544269746D617092000000424D92000000000000003E000000280000000E00
      0000150000000100010000000000540000000000000000000000020000000000
      000000000000FFFFFF00FFFC0000FFFC0000FFFC0000FF3C0000FEDC0000FEDC
      0000FEDC0000E6DC0000EADC0000EF3C0000EFFC0000EFFC0000EFFC0000C3FC
      0000EFFC0000EFFC0000FFFC0000FFFC0000FFFC0000FFFC0000FFFC0000}
    Transparent = True
  end
  object Label12: TLabel
    Left = 242
    Top = 170
    Width = 189
    Height = 13
    Caption = #1041#1072#1079#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1090#1077#1084#1087#1077#1088#1072#1090#1091#1088#1099' ('#1085'.'#1091'.)'
    WordWrap = True
  end
  object DescriptionEdit: TEdit
    Left = 8
    Top = 24
    Width = 189
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    MaxLength = 120
    ParentFont = False
    TabOrder = 0
    Text = 'DescriptionEdit'
  end
  object IndexEdit: TEdit
    Left = 8
    Top = 72
    Width = 189
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    MaxLength = 24
    ParentFont = False
    TabOrder = 1
    Text = 'Edit1'
  end
  object VEdit: TNumberEdit
    Left = 8
    Top = 120
    Width = 189
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    NumberType = Double
  end
  object SKOEdit: TNumberEdit
    Left = 8
    Top = 176
    Width = 189
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Text = '0'
    NumberType = Double
  end
  object EVOEdit: TNumberEdit
    Left = 476
    Top = 22
    Width = 67
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Text = '0'
    NumberType = Double
  end
  object ESREdit: TNumberEdit
    Left = 476
    Top = 54
    Width = 67
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = '0'
    NumberType = Double
  end
  object ESOEdit: TNumberEdit
    Left = 476
    Top = 84
    Width = 67
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = '0'
    NumberType = Double
  end
  object ESPREdit: TNumberEdit
    Left = 476
    Top = 110
    Width = 67
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    Text = '0'
    NumberType = Double
  end
  object CalibrationPicker: TDateTimePicker
    Left = 4
    Top = 218
    Width = 125
    Height = 24
    CalAlignment = dtaLeft
    Date = 38209.8150136343
    Time = 38209.8150136343
    DateFormat = dfShort
    DateMode = dmComboBox
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    Kind = dtkDate
    ParseInput = False
    ParentFont = False
    TabOrder = 10
  end
  object EndPicker: TDateTimePicker
    Left = 154
    Top = 218
    Width = 145
    Height = 24
    CalAlignment = dtaLeft
    Date = 38209.8150136343
    Time = 38209.8150136343
    DateFormat = dfShort
    DateMode = dmComboBox
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    Kind = dtkDate
    ParseInput = False
    ParentFont = False
    TabOrder = 11
  end
  object ValidCheckBox: TCheckBox
    Left = 334
    Top = 218
    Width = 203
    Height = 19
    Caption = #1044#1077#1081#1089#1090#1074#1080#1090#1077#1083#1100#1085#1099' '#1085#1072' '#1076#1072#1085#1085#1099#1081' '#1084#1086#1084#1077#1085#1090
    TabOrder = 12
  end
  object ESOOEdit: TNumberEdit
    Left = 476
    Top = 136
    Width = 67
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Text = '0'
    NumberType = Double
  end
  object tEdit: TNumberEdit
    Left = 478
    Top = 164
    Width = 67
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    Text = '0'
    NumberType = Double
  end
  object Button1: TBitBtn
    Left = 2
    Top = 250
    Width = 271
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1080' '#1074#1099#1081#1090#1080
    TabOrder = 13
    Kind = bkOK
  end
  object Button2: TBitBtn
    Left = 278
    Top = 250
    Width = 271
    Height = 25
    Caption = #1042#1099#1081#1090#1080' '#1073#1077#1079' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1081
    TabOrder = 14
    Kind = bkCancel
  end
end
