object SelectProverForm: TSelectProverForm
  Left = 278
  Top = 182
  ActiveControl = ProversGrid
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1042#1099#1073#1086#1088' '#1058#1055#1059
  ClientHeight = 295
  ClientWidth = 604
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000E000000000000000EEEEEEEEEEE00000EAAAAAAAAAAE0000EEE
    EEEEEEEAAE000E000000000EAAE0000000000000EAE0000000000000EAE00000
    00000000EAE0000000000000EAE00E000000000EAAE00EEEEEEEEEEAAE000EAA
    AAAAAAAAE0000EEEEEEEEEEE00000E000000000000000000000000000000FFFF
    0000BFFF0000800F00008007000080030000BFE10000FFF10000FFF10000FFF1
    0000FFF10000BFE100008003000080070000800F0000BFFF0000FFFF0000}
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 4
    Width = 66
    Height = 13
    Caption = #1057#1087#1080#1089#1086#1082' '#1058#1055#1059':'
  end
  object ProversGrid: TStringGrid
    Left = 4
    Top = 20
    Width = 597
    Height = 177
    ColCount = 7
    DefaultColWidth = 20
    DefaultRowHeight = 18
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect]
    TabOrder = 0
    OnDblClick = ProversGridDblClick
    ColWidths = (
      20
      126
      118
      57
      67
      39
      158)
  end
  object OkButton: TBitBtn
    Left = 4
    Top = 266
    Width = 207
    Height = 25
    Caption = #1042#1099#1073#1088#1072#1090#1100
    TabOrder = 5
    Kind = bkOK
  end
  object CancelButton: TBitBtn
    Left = 224
    Top = 266
    Width = 177
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 6
    Kind = bkCancel
  end
  object ViewButton: TBitBtn
    Left = 4
    Top = 202
    Width = 207
    Height = 25
    Caption = #1055#1088#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1074#1089#1077' '#1076#1072#1085#1085#1099#1077' '#1058#1055#1059'...'
    TabOrder = 1
    OnClick = ViewButtonClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888800
      88888888888884E60888887000004E6C008888788884E6C88088887FFF4E6C88
      8088887007F6C88F8088878E808088FF80887FE8E8088FFF80887EFE8E08FFFF
      80887FFFE808FFFF808887FE8088FFFF80888877088FFFF8808888788FFFFF00
      0088887FFFFFFF7F7888887FFFFFFF7788888877777777788888}
    Margin = 4
  end
  object EditButton: TBitBtn
    Left = 4
    Top = 230
    Width = 207
    Height = 25
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1058#1055#1059'...'
    TabOrder = 2
    OnClick = EditButtonClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFF0000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4FFCDB4FF
      CDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
      FFFFFFFFFFFF000000FFCDB4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4FFCDB4FF
      CDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
      FFFFFFFFFFFF000000FFCDB4FFFFFF0000000000000000FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4000000FF
      FFFFFFFFFF000000FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
      FFFFFFFFFFFF00000000000000FFFF000000FFFFFF000000FFFFFFFFFFFFFFFF
      FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF00000000FFFF00FFFF00
      0000000000FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
      FFFFFF00000000FFFF00FFFF0000000000000000000000000000000000000000
      00000000000000000000FFFFFFFFFFFF00000000FFFF00FFFF000000000000FF
      8548FF8548FF8548FF8548FF8548FF8548FF8548FF8548000000FFFFFF000000
      FFFFFF00FFFF0000000000000000000000000000000000000000000000000000
      00000000000000000000000000FFFFFF00FFFF000000000000FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
      000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFF000000FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    Margin = 4
  end
  object AddButton: TBitBtn
    Left = 224
    Top = 202
    Width = 177
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1058#1055#1059' '#1074' '#1089#1087#1080#1089#1086#1082'...'
    TabOrder = 3
    OnClick = AddButtonClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      88888888880000888888888887AAA0888888888887AAA0888888888887AAA088
      8888888887AAA0888888877777AAA00000887AAAAAAAAAAAAA087AAAAAAAAAAA
      AA087AAAAAAAAAAAAA08877777AAA0000088888887AAA0888888888887AAA088
      8888888887AAA0888888888887AAA08888888888887778888888}
    Margin = 4
  end
  object DeleteButton: TBitBtn
    Left = 224
    Top = 230
    Width = 177
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1058#1055#1059'...'
    TabOrder = 4
    OnClick = DeleteButtonClick
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888888888888888888888888888888888888888888888
      8888888888888888888880000000000000887999999999999908799999999999
      9908799999999999990887777777777777888888888888888888888888888888
      8888888888888888888888888888888888888888888888888888}
    Margin = 4
  end
end
